import Link from "next/link";
import Image from "next/image";
//Components
import SearchInput from "../SearchInput/SearchInput";

type Props = {
  setQuery?: React.Dispatch<React.SetStateAction<string>>;
};

const Header = ({ setQuery }: Props) => (
  <div className="sticky flex top-0 z-40 w-full h-24 bg-gradient-to-b from-blue-200 to-cyan-200">
    <div className="flex items-center justify-between w-full h-full max-w-7xl m-auto px-4">
      <Link href={"/"}>
        <div className="flex cursor-pointer">
          <div className="invisible md:visible">
            <Image
              width={80}
              height={50}
              src={"/logo.png"}
              alt="restaurant-logo"
            />
          </div>{" "}
          <div className="max-w-7xl m-auto px-4 text-3xl font-extrabold invisible md:visible">
            RESTO
          </div>
          <div className="absolute md:invisible">
            <Image
              width={50}
              height={50}
              src={"/logo.png"}
              alt="restaurant-logo-small"
            />
          </div>
        </div>
      </Link>
      {setQuery ? (
        <div className="relative flex items-center">
          <SearchInput setQuery={setQuery} />
        </div>
      ) : null}
    </div>
  </div>
);

export default Header;
