import Image from "next/image";
// Helpers
import { calcTime, convertMoney } from "@/helpers";
// Components
import Thumb from "../Thumb/Thumb";
import Pill from "../Pill/Pill";
// Types
import { Menus } from "@/api/detailTypes";

type Props = {
  thumbUrl: string;
  name: string;
  description: string;
  city: string;
  address: string;
  rating: number;
  menus: Menus;
};
const RestaurantInfo = ({
  thumbUrl,
  name,
  description,
  city,
  address,
  rating,
  menus,
}: Props) => {
  console.log(thumbUrl);
  return (
    <div className="relative w-full h-auto p-4">
      <div className="relative h-full min-h-128 flex flex-col md:flex-row max-w-7xl p-4 m-auto z-10 rounded-xl bg-zinc-800 bg-opacity-90">
        <div className="relative w-full h-96 md:h-auto md:w-1/3">
          <Thumb imgUrl={thumbUrl} />
          <div className="absolute top-4 left-4 rounded-full bg-white w-10 h-10 flex justify-center items-center text-black text-sm font-bold">
            {" "}
            {rating}
          </div>
        </div>
        <div className="text-white px-0 py-4 md:py-0 text-center md:text-left md:px-8 w-full md:w-2/3">
          <h2 className="text-2xl md:text-4xl font-bold pb-4">{name}</h2>
          <h3 className="text-lg font-bold">Ringkasan</h3>
          <p className="mb-8 text-sm md:text-lg">{description}</p>{" "}
          <h3 className="text-lg font-bold">Alamat</h3>
          <p className="mb-8 text-sm md:text-lg">{address + " - " + city}</p>
          <div className="flex">
            <div>
              <h3 className="text-lg font-bold">Daftar Makanan</h3>
              <div>
                {menus.foods.map((menu) => (
                  <p key={menu.name}>{menu.name}</p>
                ))}
              </div>{" "}
            </div>{" "}
            <div className="ml-10">
              <h3 className="text-lg font-bold">Daftar Minuman</h3>
              <div>
                {menus.drinks.map((menu) => (
                  <p key={menu.name}>{menu.name}</p>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Image
        priority
        placeholder="blur"
        blurDataURL="/placeholder.jpg"
        objectFit="cover"
        objectPosition="center"
        layout="fill"
        src={thumbUrl}
        alt="thumb"
      />
    </div>
  );
};

export default RestaurantInfo;
