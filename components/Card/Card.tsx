// Components
import Thumb from "../Thumb/Thumb";
import Link from "next/link";
import { Button } from "@mantine/core";

type Props = {
  imgUrl: string;
  title: string;
  subtitle?: string;
  restaurantId: string;
};

const Card = ({ imgUrl, title, subtitle, restaurantId }: Props) => (
  <div className="h-80">
    <div className="relative h-full">
      <Thumb imgUrl={imgUrl} />
      <div className="absolute w-full bottom-0 px-4 py-2  rounded-b-xl bg-zinc-800">
        <h2 className="text-cyan-200 text-center text-sm truncate">{title}</h2>
        {subtitle ? (
          <p className="text-cyan-400 text-center text-xs truncate">
            {subtitle}
          </p>
        ) : null}
      </div>
      <div className="flex justify-center absolute w-full bottom-16 ">
        {" "}
        <Button
          className="opacity-90"
          component={Link}
          href={`/${restaurantId}`}
        >
          Lihat Detail
        </Button>
      </div>
    </div>
  </div>
);

export default Card;
