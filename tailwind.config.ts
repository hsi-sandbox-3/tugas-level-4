import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    gridTemplateColumns: {
      "auto-fill": "repeat(auto-fill,minmax(200px,1fr))",
    },
    extend: {
      height: { 128: "40rem" },
    },
    fontFamily: { raleway: ["Raleway", "sans-serif"] },
    minHeight: {
      128: "40rem",
    },
  },
  plugins: [],
};
export default config;
