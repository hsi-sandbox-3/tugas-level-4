/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: "http",
        hostname: "image.tmdb.org",
        port: "",
        pathname: "/**",
      },
      {
        protocol: "https",
        hostname: "restaurant-api.dicoding.dev",
        port: "",
        pathname: "/**",
      },
    ],
  },
};

export default nextConfig;
