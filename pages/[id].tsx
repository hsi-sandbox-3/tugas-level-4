import { BASE_URL, IMAGE_BASE_URL } from "@/config";
// Basic fetch
import { basicFetch } from "@/api/fetchFunctions";
// Components
import Header from "@/components/Header/Header";
import Breadcrumb from "@/components/Breadcrumb/Breadcrumb";
import RestaurantInfo from "@/components/RestaurantInfo/RestaurantInfo";
import Grid from "@/components/Grid/Grid";
import Card from "@/components/Card/Card";

// Types
import type { GetStaticPaths, GetStaticProps, NextPage } from "next";
import type {
  Detail,
  Restaurant,
  Category,
  CustomerReview,
  Menus,
} from "@/api/detailTypes";

type Props = {
  restaurant: Restaurant;
};

const Restaurant: NextPage<Props> = ({ restaurant }) => (
  <main>
    <Header />
    <Breadcrumb title={restaurant.name} />
    <RestaurantInfo
      thumbUrl={
        restaurant.id ? IMAGE_BASE_URL + restaurant.pictureId : "/no_image.jpg"
      }
      name={restaurant.name}
      description={restaurant.description}
      city={restaurant.city}
      address={restaurant.address}
      rating={restaurant.rating}
      menus={restaurant.menus}
    />
  </main>
);

export default Restaurant;

export const getStaticProps: GetStaticProps = async (context) => {
  const id = context.params?.id as string;

  const restaurant = (await basicFetch<Detail>(BASE_URL + `/detail/${id}`))
    .restaurant;

  return {
    props: {
      restaurant,
    },
    revalidate: (60 * 60) & 24, // Re-build page every 24 hours
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [],
    fallback: "blocking",
  };
};
