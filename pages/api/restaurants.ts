// API Urls
import { BASE_URL } from "@/config";
// Basic fetch function
import { basicFetch } from "@/api/fetchFunctions";
// Types
import type { NextApiRequest, NextApiResponse } from "next";
import type { Restaurants } from "@/api/types";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Restaurants>
) {
  const { search } = req.query; // Grab search params

  const endpoint = search
    ? `${BASE_URL}/search?q=${search}`
    : `${BASE_URL}/list`;

  const data = await basicFetch<Restaurants>(endpoint);

  res.status(200).json(data);
}
