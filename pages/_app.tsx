import "@/styles/globals.css";
import "@mantine/core/styles.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import type { AppProps } from "next/app";
import { createTheme, MantineProvider } from "@mantine/core";

const queryClient = new QueryClient();
export default function App({ Component, pageProps }: AppProps) {
  return (
    <MantineProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <Component {...pageProps} />;
      </QueryClientProvider>{" "}
    </MantineProvider>
  );
}

const theme = createTheme({
  /** Put your mantine theme override here */
});
