import React from "react";
import Link from "next/link";

import type { NextPage } from "next";
// Fecth hook
import { useFetchRestaurants } from "@/api/fetchHooks";
// Config
import { IMAGE_BASE_URL } from "@/config";
//Components
import Header from "@/components/Header/Header";
import Hero from "@/components/Hero/Hero";
import Grid from "@/components/Grid/Grid";
import Card from "@/components/Card/Card";
import Spinner from "@/components/Spinner/Spinner";

const Home: NextPage = () => {
  const [query, setQuery] = React.useState("");

  const { data, isLoading, isFetching, error } = useFetchRestaurants(query);

  if (error) return <div>Oh nooooooooo something went wrong!</div>;

  return (
    <main className="relative h-screen overflow-y-scroll">
      <Header setQuery={setQuery} />

      <Grid
        className="p-4 max-w-7xl m-auto"
        title={
          query
            ? "Search Results : " + (data?.founded ? data?.founded : 0)
            : "Restoran Populer"
        }
      >
        {data
          ? data.restaurants.map((restaurant) => (
              <div className="cursor-pointer hover:opacity-80 duration-300">
                <Card
                  imgUrl={
                    restaurant.pictureId
                      ? IMAGE_BASE_URL + restaurant.pictureId
                      : "/no_image.jpg"
                  }
                  title={`${restaurant.name} - ${restaurant.city}`}
                  restaurantId={restaurant.id}
                />
              </div>
              // </Link>
            ))
          : null}
      </Grid>

      {isLoading || isFetching ? <Spinner /> : null}
    </main>
  );
};

export default Home;
