const BASE_URL: string = "https://restaurant-api.dicoding.dev";

const IMAGE_BASE_URL: string = `${BASE_URL}/images/large/`;

export { IMAGE_BASE_URL, BASE_URL };
