import { useQuery } from "@tanstack/react-query";
// Fecth function
import { fetchRestaurants } from "./fetchFunctions";

export const useFetchRestaurants = (search: string) => {
  return useQuery({
    queryKey: ["restaurants", search],
    queryFn: () => fetchRestaurants(search),
    refetchOnWindowFocus: false,
  });
};
