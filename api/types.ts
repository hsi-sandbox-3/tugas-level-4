export type RestaurantDetail = {
  backdrop_path: string;
  id: number;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  title: string;
  vote_average: number;
  vote_count: number;
  budget: number;
  runtime: number;
  revenue: number;
  release_date: string;
};

export type Restaurant = {
  id: string;
  name: string;
  description: string;
  pictureId: number;
  city: string;
  rating: number;
};

export type Restaurants = {
  count: number;
  founded: number;
  restaurants: Restaurant[];
};
