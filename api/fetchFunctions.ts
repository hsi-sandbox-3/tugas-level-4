import { Restaurants } from "./types";

export const basicFetch = async <returnType>(
  endpoint: string
): Promise<returnType> => {
  const response = await fetch(endpoint);

  if (!response.ok) throw new Error("Error!");

  const datas = await response.json();

  return datas;
};

// Fecth functions
export const fetchRestaurants = async (search = ""): Promise<Restaurants> => {
  return await basicFetch<Restaurants>(`/api/restaurants?search=${search}`);
};
