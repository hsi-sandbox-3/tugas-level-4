export type Detail = {
  error: boolean;
  message: string;
  restaurant: Restaurant;
};

export type Restaurant = {
  id: string;
  name: string;
  description: string;
  city: string;
  address: string;
  pictureId: string;
  categories: Category[];
  menus: Menus;
  rating: number;
  customerReviews: CustomerReview[];
};

export type Category = {
  name: string;
};

export type CustomerReview = {
  name: string;
  review: string;
  date: string;
};

export type Menus = {
  foods: Category[];
  drinks: Category[];
};
